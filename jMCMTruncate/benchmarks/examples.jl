# Open julia in mcmfa/jMCMTruncate/benchmarks
# Or anywhere in your system and then enter bash mode with `;` and use usual cd
# to quit the mode, use backspace

# If updates in AdderGraphs are needed, type `]update`
# and backspace to quit the mode
# or type the following lines
using Pkg
Pkg.update

# Include the file with benchmark() function
include("benchmarks_tests.jl")

# Examples:
A1 = benchmarks(10, wl_in=5, epsilon=0)
A2 = benchmarks(10, wl_in=5, epsilon=256)
A3 = benchmarks(24, wl_in=5, epsilon=0)


# Use println(write_addergraph(A)) to print the addergraph in flopoco format
# Use println(write_addergraph_truncations(A)) to print the addergraph truncations in flopoco format

println(write_addergraph(A1))
println(write_addergraph_truncations(A1))
println(write_addergraph(A2))
println(write_addergraph_truncations(A2))
println(write_addergraph(A3))
println(write_addergraph_truncations(A3))

pretty_print_get_maximal_error(A1, wlIn=8, msbIn=7, lsbIn=0, verbose=true)
pretty_print_get_maximal_error(A2, wlIn=8, msbIn=7, lsbIn=0, verbose=true)
pretty_print_get_maximal_error(A3, wlIn=8, msbIn=7, lsbIn=0, verbose=true)
