using AdderGraphs, JuMP, Gurobi
include("../../jMCM/src/jMCM.jl")
using .jMCM

include("../../jAGTruncate/src/jAGTruncate.jl")
using .jAGTruncate


function benchmarks(which_benchmark::Int, runnb::Int)
    benchmark_naming = "_mcm_without_ws_error_$(runnb)"
    use_rpag = false
    msb_in = -1
    lsb_in = -8
    output_error_init = output_error = float(-1)#float(-1) #float(0)
    all_benchmarks = Vector{Tuple{String, String, Int, Int, Int, Vector{Int}, Vector{Int}}}()
    open("benchmarks.csv") do file
        lines = readlines(file)
        for line in lines[2:end]
            line_data = split(line, ",")
            push!(all_benchmarks,
                (line_data[1], # name
                line_data[2], # filter_type
                parse(Int, line_data[3]), # wordlength
                parse(Int, line_data[4]), # number_of_coefficients
                parse(Int, line_data[5]), # number_of_unique_coefficients
                parse.(Int, split(line_data[6])), # coefficients
                parse.(Int, split(line_data[7]))) # unique_coefficients
            )
        end
    end
    println("\n\n\n\n----- Mock run -----\n\n")
    C = [7, 19, 31]
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    if output_error_init < 0
        if !isempty(oddabsC)
            output_error = 2.0^(lsb_in)*2.0^(round(log2(maximum(oddabsC)), RoundDown))
        else
            output_error = float(0)
        end
    end
    output_errors = Dict{Int, Float64}([oddcoeff => output_error for oddcoeff in oddabsC])
    for i in 1:length(oddabsC)
        oddcoeff = oddabsC[i]
        for coeff in C
            if odd(abs(coeff)) == oddcoeff
                output_errors[oddcoeff] = min(output_errors[oddcoeff], output_error/(div(abs(coeff), oddcoeff)))
            end
        end
    end

    A = AdderGraph()
    if use_rpag
        A = rpag(oddabsC)
        addernodes = get_nodes(A)
        for addernode in addernodes
            sort!(addernode.inputs, by=x->x.shift, rev=true)
        end
    end

    model = Model(Gurobi.Optimizer)
    set_optimizer_attributes(model, "Threads" => 4)
    #set_optimizer_attributes(model, "CPXPARAM_Threads" => 4)
    set_silent(model)
    set_time_limit_sec(model, 600)
    A = mcm(model, oddabsC, verbose=true, addergraph_warmstart=A, use_all_adders=false, nb_adders_lb=length(A))
    model_truncate = Model(Gurobi.Optimizer)
    set_optimizer_attributes(model_truncate, "Threads" => 4)
    #set_optimizer_attributes(model_truncate, "CPXPARAM_Threads" => 4)
    set_silent(model_truncate)
    truncate!(A, model_truncate, msb_in=msb_in, lsb_in=lsb_in, output_errors_dict=output_errors)


    benchmark_info = all_benchmarks[which_benchmark]
    println("\n\n\n\n----- Problem $(benchmark_info[1]) -----\n\n")
    C = copy(benchmark_info[6])
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    if output_error_init < 0
        if !isempty(oddabsC)
            output_error = 2.0^(lsb_in)*2.0^(round(log2(maximum(oddabsC)), RoundDown))
        else
            output_error = float(0)
        end
    end
    output_errors = Dict{Int, Float64}([oddcoeff => output_error for oddcoeff in oddabsC])
    for i in 1:length(oddabsC)
        oddcoeff = oddabsC[i]
        for coeff in C
            if odd(abs(coeff)) == oddcoeff
                output_errors[oddcoeff] = min(output_errors[oddcoeff], output_error/(div(abs(coeff), oddcoeff)))
            end
        end
    end

    A = AdderGraph()
    if use_rpag
        if !isempty(oddabsC)
            A = rpag(oddabsC)
            addernodes = get_nodes(A)
            for addernode in addernodes
                sort!(addernode.inputs, by=x->x.shift, rev=true)
            end
        end
        open("results$(benchmark_naming).txt", "a") do file
            if isvalid(A)
                if isempty(oddabsC)
                    write(file, "$(benchmark_info[1]) -- 0")
                elseif has_values(model)
                    write(file, "$(benchmark_info[1]) -- $(length(A))")
                end
            else
                write(file, "$(benchmark_info[1]) -- error")
            end
            write(file, " -- rpag\n")
        end
    end

    model = Model(Gurobi.Optimizer)
    #set_optimizer_attributes(model, "SolFiles" => "$(@__DIR__)/solutions/$(benchmark_info[1])$(benchmark_naming)")
    set_optimizer_attributes(model, "Threads" => 4)
    current_intfeastol = get_optimizer_attribute(model, "IntFeasTol")
    #set_optimizer_attributes(model, "CPXPARAM_Threads" => 4)

    #write_model="$(@__DIR__)/models/$(benchmark_info[1])$(benchmark_naming)"
    #set_silent(model)
    set_time_limit_sec(model, 1800)
    tmp_time = @timed ag = mcm(model, oddabsC, verbose=true, addergraph_warmstart=A, use_all_adders=false, nb_adders_lb=length(A))
    solving_time = tmp_time[2]
    while isempty(ag) && has_values(model) && current_intfeastol >= 1e-9
        model = Model(Gurobi.Optimizer)
        set_optimizer_attributes(model, "Threads" => 4)
        current_intfeastol /= 10
        set_optimizer_attributes(model, "IntFeasTol" => current_intfeastol)
        set_time_limit_sec(model, max(1800-solving_time, 0))
        tmp_time2 = @timed ag = mcm(model, oddabsC, verbose=true, addergraph_warmstart=A, use_all_adders=false, nb_adders_lb=length(A))
        solving_time += tmp_time2[2]
    end
    addernodes = get_nodes(ag)
    for addernode in addernodes
        sort!(addernode.inputs, by=x->x.shift, rev=true)
    end

    open("results$(benchmark_naming).txt", "a") do file
        if isvalid(ag)
            if isempty(oddabsC)
                write(file, "$(benchmark_info[1]) -- 0")
            elseif has_values(model) && !isempty(ag)
                write(file, "$(benchmark_info[1])")
                addernodes = get_nodes(ag)
                for addernode in addernodes
                    sort!(addernode.inputs, by=x->x.shift, rev=true)
                end
                model_truncate = Model(Gurobi.Optimizer)
                set_optimizer_attributes(model_truncate, "Threads" => 4)
                #set_optimizer_attributes(model_truncate, "CPXPARAM_Threads" => 4)
                set_silent(model_truncate)
                truncate!(ag, model_truncate, msb_in=msb_in, lsb_in=lsb_in, output_errors_dict=output_errors)
                if has_values(model_truncate)
                    write(file, " -- $(round(Int, objective_value(model_truncate)))")
                end
                if termination_status(model) != MOI.OPTIMAL
                    write(file, " -- $(length(ag))")
                    write(file, "*")
                else
                    write(file, " -- $(length(ag))")
                    write(file, " -- $(solving_time)")
                end
            elseif has_values(model)
                write(file, "$(benchmark_info[1]) -- error")
            else
                write(file, "$(benchmark_info[1]) -- TO")
            end
        else
            write(file, "$(benchmark_info[1]) -- error")
        end
        write(file, "\n")
    end
    return nothing
end

benchmarks(parse(Int, ARGS[1]), parse(Int, ARGS[2]))
