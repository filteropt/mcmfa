mutable struct Result
    name::String
    wordlength::Int
    nb_coeff::Int
    mcm_na::Union{Nothing, Int}
    rpag_na::Union{Nothing, Int}
    fa_na::Union{Nothing, Int}
    fa_rpag_na::Union{Nothing, Int}
    mcm_fa::Union{Nothing, Int}
    rpag_fa::Union{Nothing, Int}
    fa_fa::Union{Nothing, Int}
    fa_rpag_fa::Union{Nothing, Int}
    mcm_error_fa::Union{Nothing, Int}
    rpag_error_fa::Union{Nothing, Int}
    fa_error_fa::Union{Nothing, Int}
    proof_fa_fa::Union{Nothing, Bool}
    proof_fa_rpag_fa::Union{Nothing, Bool}
    proof_fa_error::Union{Nothing, Bool}
end

function Result(name::String, wordlength::Int, nb_coeff::Int)
    return Result(name, wordlength, nb_coeff, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing, nothing)
end

function main()
    results = Vector{Result}()
    open("benchmarks.csv") do file
        lines = readlines(file)
        for line in lines[2:end]
            if isempty(line)
                continue
            end
            line_data = split(line, ",")
            push!(results,
                Result(string(line_data[1]), # name
                parse(Int, line_data[3]), # wordlength
                parse(Int, line_data[5])) # number_of_unique_coefficients
            )
        end
    end
    # open("results.txt") do file
    #     lines = readlines(file)
    #     for line in lines
    #         line_vec = split(line, " -- ")
    #         if string(line_vec[end]) == "TO"
    #             continue
    #         end
    #         if string(line_vec[end]) == "error"
    #             continue
    #         end
    #         i = 1
    #         while results[i].name != line_vec[1]
    #             i+=1
    #         end
    #         if string(line_vec[2]) == "0"
    #             results[i].proof_fa_fa = true
    #             results[i].fa_fa = 0
    #             results[i].fa_na = 0
    #             continue
    #         end
    #         if string(line_vec[2][end]) == "*"
    #             results[i].proof_fa_fa = false
    #             results[i].fa_fa = parse(Int, line_vec[2][1:(end-1)])
    #         else
    #             results[i].proof_fa_fa = true
    #             results[i].fa_fa = parse(Int, line_vec[2])
    #         end
    #         results[i].fa_na = parse(Int, line_vec[3])
    #     end
    # end
    open("results_with_rpag.txt") do file
        lines = readlines(file)
        for line in lines
            if isempty(line)
                continue
            end
            line_vec = split(line, " -- ")
            if string(line_vec[end]) == "TO"
                continue
            end
            if string(line_vec[end]) == "error"
                continue
            end
            i = 1
            while results[i].name != line_vec[1]
                i+=1
            end
            if string(line_vec[2]) == "0"
                results[i].proof_fa_fa = true
                results[i].rpag_fa = 0
                results[i].rpag_na = 0
                results[i].fa_rpag_fa = 0
                results[i].fa_rpag_na = 0
                results[i].proof_fa_rpag_fa = true
                continue
            end
            if string(line_vec[end]) == "rpag"
                results[i].rpag_fa = parse(Int, line_vec[2])
                results[i].rpag_na = parse(Int, line_vec[3])
            else
                if string(line_vec[2][end]) == "*"
                    results[i].proof_fa_rpag_fa = false
                    if string(line_vec[2][end-1]) == "*"
                        results[i].fa_rpag_fa = parse(Int, line_vec[2][1:(end-2)])
                    else
                        results[i].fa_rpag_fa = parse(Int, line_vec[2][1:(end-1)])
                    end
                else
                    results[i].proof_fa_rpag_fa = true
                    results[i].fa_rpag_fa = parse(Int, line_vec[2])
                end
                results[i].fa_rpag_na = parse(Int, line_vec[3])
            end
        end
    end

    open("results_mcm_without_ws.txt") do file
        lines = readlines(file)
        for line in lines
            if isempty(line)
                continue
            end
            line_vec = split(line, " -- ")
            if string(line_vec[end]) == "TO"
                continue
            end
            if string(line_vec[end]) == "error"
                continue
            end
            i = 1
            while results[i].name != line_vec[1]
                i+=1
            end
            if string(line_vec[2]) == "0"
                results[i].mcm_na = 0
                results[i].mcm_fa = 0
                continue
            end
            results[i].mcm_na = parse(Int, line_vec[3])
            results[i].mcm_fa = parse(Int, line_vec[2])
        end
    end

    open("results_mcm_without_ws_error.txt") do file
        lines = readlines(file)
        for line in lines
            if isempty(line)
                continue
            end
            line_vec = split(line, " -- ")
            if string(line_vec[end]) == "TO"
                continue
            end
            if string(line_vec[end]) == "error"
                continue
            end
            i = 1
            while results[i].name != line_vec[1]
                i+=1
            end
            if string(line_vec[2]) == "0"
                results[i].mcm_error_fa = 0
                continue
            end
            results[i].mcm_error_fa = parse(Int, line_vec[2])
        end
    end

    open("results_with_rpag_error.txt") do file
        lines = readlines(file)
        for line in lines
            if isempty(line)
                continue
            end
            line_vec = split(line, " -- ")
            if string(line_vec[end]) == "TO"
                continue
            end
            if string(line_vec[end]) == "error"
                continue
            end
            i = 1
            while results[i].name != line_vec[1]
                i+=1
            end
            if string(line_vec[2]) == "0"
                results[i].proof_fa_error = true
                results[i].fa_error_fa = 0
                results[i].rpag_error_fa = 0
                continue
            end
            if string(line_vec[end]) == "rpag"
                results[i].rpag_error_fa = parse(Int, line_vec[2])
            else
                if string(line_vec[2][end]) == "*"
                    results[i].proof_fa_error = false
                    if string(line_vec[2][end-1]) == "*"
                        results[i].fa_error_fa = parse(Int, line_vec[2][1:(end-2)])
                    else
                        results[i].fa_error_fa = parse(Int, line_vec[2][1:(end-1)])
                    end
                else
                    results[i].proof_fa_error = true
                    results[i].fa_error_fa = parse(Int, line_vec[2])
                end
            end
        end
    end


    function ifnothing(a, proof=true)
        if a == nothing
            return ""
        end
        return string(a)*(proof == false ? "*" : "")
    end
    function percentnothing(a, b, c)
        if c == nothing
            return ""
        end
        if a == nothing && b == nothing
            return ""
        end
        d = a
        if b != nothing
            d = b
            if a != nothing
                d = a#min(a,b)
            end
        end
        if d == 0
            return "0"
        end
        return string((d-c)/d)#*"%"
    end

    open("results_data.csv", "w") do file
        write(file, ",,,,#A,#A,,,#FA,#FA,%FA gain,%FA gain,error,error,error\n,,,#A,MCM_FA,MCM_FA,#FA,#FA,MCM_FA,MCM_FA,MCM_FA,MCM_FA,MCM, MCM_FA, %Gain\nBenchmark,WL,#Coeff,MCM/RPAG,W RPAG,NoW,MCM,RPAG,W RPAG,NoW,W RPAG,NoW,,\n")
        for result in results
            write(file, "$(result.name), $(result.wordlength), $(result.nb_coeff),")
            write(file, "$(ifnothing(result.mcm_na) == "" ? ifnothing(result.rpag_na)*"*" : ifnothing(result.mcm_na)),")
            write(file, "$(ifnothing(result.fa_rpag_na)), $(ifnothing(result.fa_na)),")
            write(file, "$(ifnothing(result.mcm_fa) == "" ? ifnothing(result.rpag_fa)*"*" : ifnothing(result.mcm_fa)), $(ifnothing(result.rpag_fa)),")
            write(file, "$(ifnothing(result.fa_rpag_fa, result.proof_fa_rpag_fa)), $(ifnothing(result.fa_fa, result.proof_fa_fa)),")
            write(file, "$(percentnothing(result.mcm_fa, result.rpag_fa, result.fa_rpag_fa)),$(percentnothing(result.mcm_fa, result.rpag_fa, result.fa_fa)),")
            write(file, "$(ifnothing(result.mcm_error_fa) == "" ? ifnothing(result.rpag_error_fa)*"*" : ifnothing(result.mcm_error_fa)), $(ifnothing(result.fa_error_fa, result.proof_fa_error)),$(percentnothing(result.mcm_error_fa, result.rpag_error_fa, result.fa_error_fa))")
            write(file, ",$(percentnothing(result.fa_rpag_fa, nothing, result.fa_error_fa))\n")
        end
    end
end

main()
