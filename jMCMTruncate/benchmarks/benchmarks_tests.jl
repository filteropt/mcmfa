using AdderGraphs, JuMP, Gurobi
include((@__DIR__)*"/../src/utils.jl")
include((@__DIR__)*"/../src/ilp1.jl")
include((@__DIR__)*"/../src/mcm.jl")
include((@__DIR__)*"/../src/rpag.jl")

include((@__DIR__)*"/../../jAGTruncate/src/jAGTruncate.jl")
using .jAGTruncate


function benchmarks(which_benchmark::Int; wl_in::Int,#msb_in::Int=-1, lsb_in::Int=-8,
                    epsilon::Union{Vector{Int}, Int}=Vector{Int}(),
                    coefficients_shift::Int=0,
                    debug::Bool=false,
                    verbose::Bool=true)
    verbose = debug || verbose
    msb_in=wl_in
    lsb_in=0
    benchmark_naming = "_with_error"
    if isempty(epsilon)
        println("Need epsilon")
        return AdderGraph()
    end

    all_benchmarks = Vector{Tuple{String, String, Int, Int, Int, Vector{Int}, Vector{Int}}}()
    open((@__DIR__)*"/benchmarks.csv") do file
        lines = readlines(file)
        for line in lines[2:end]
            line_data = split(line, ",")
            push!(all_benchmarks,
                (line_data[1], # name
                line_data[2], # filter_type
                parse(Int, line_data[3]), # wordlength
                parse(Int, line_data[4]), # number_of_coefficients
                parse(Int, line_data[5]), # number_of_unique_coefficients
                parse.(Int, split(line_data[6])), # coefficients
                parse.(Int, split(line_data[7]))) # unique_coefficients
            )
        end
    end

    # ------------------- Mock run ------------------- #
    debug && println("\n\n\n\n----- Mock run -----\n\n")
    C = [7, 19, 31]
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    output_error = 2.0^(lsb_in)*2.0^(round(log2(maximum(oddabsC)), RoundDown))
    output_errors = Dict{Int, Float64}([oddcoeff => output_error for oddcoeff in oddabsC])
    for i in 1:length(oddabsC)
        oddcoeff = oddabsC[i]
        for coeff in C
            if odd(abs(coeff)) == oddcoeff
                output_errors[oddcoeff] = min(output_errors[oddcoeff], output_error/(div(abs(coeff), oddcoeff)))
            end
        end
    end

    model = Model(Gurobi.Optimizer)
    set_optimizer_attributes(model, "Threads" => 4)
    set_optimizer_attributes(model, "PoolSolutions" => 100)
    #set_optimizer_attributes(model, "CPXPARAM_Threads" => 4)
    set_silent(model)
    set_time_limit_sec(model, 600)
    mcm_truncate(model, oddabsC, msb_in=msb_in, lsb_in=lsb_in, output_errors_dict=output_errors, verbose=debug)
    # ------------------- End mock run ------------------- #



    benchmark_info = all_benchmarks[which_benchmark]
    verbose && println("\n\n\n\n----- Problem $(benchmark_info[1]) -----\n\n")
    C = copy(benchmark_info[6])
    verbose && println("Coefficients: $C")
    if typeof(epsilon) == Int
        epsilon = repeat([epsilon], benchmark_info[4])
    end
    if length(epsilon) != benchmark_info[4]
        println("epsilon length is not consistent with the instance")
        return AdderGraph()
    end
    output_errors = Dict{Int, Float64}([C[i] => Inf for i in 1:length(C)])
    for i in 1:length(C)
        # If multiple occurences of the same coefficient we take the smallest epsilon
        output_errors[C[i]] = min(output_errors[C[i]], epsilon[i])
    end
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    verbose && println("Odd coefficients: $oddabsC")

    model = Model(Gurobi.Optimizer)
    #set_optimizer_attributes(model, "SolFiles" => "$(@__DIR__)/solutions/$(benchmark_info[1])$(benchmark_naming)")
    # set_optimizer_attributes(model, "SolutionLimit" => 1)
    set_optimizer_attributes(model, "Threads" => 4)
    set_optimizer_attributes(model, "PoolSolutions" => 100)
    #set_optimizer_attributes(model, "CPXPARAM_Threads" => 4)

    #write_model="$(@__DIR__)/models/$(benchmark_info[1])$(benchmark_naming)"
    #set_silent(model)
    set_time_limit_sec(model, 1800)
    tmp_time = @timed ag = mcm_truncate(model, C, msb_in=msb_in, lsb_in=lsb_in, output_errors_dict=output_errors, verbose=verbose)
    solving_time = tmp_time[2]




    #Check error
    # count_output_error = 0
    # input_failed_error = Vector{Tuple{Int, Int, Tuple{Float64, Float64}}}()
    # for i in 0:(2^(msb_in-lsb_in+1)-1)
    #     output_values = evaluate(ag, i, wlIn=msb_in-lsb_in+1, apply_internal_truncations=true)
    #     for output_value in get_outputs(ag)
    #         # println((abs(output_value*i - output_values[output_value])/2.0^(round(Int, log2(output_value), RoundUp)+msb_in-lsb_in+1), output_errors[output_value]))
    #         if abs(output_value*i - output_values[output_value]) > output_errors[output_value]*2^(-lsb_in)
    #             count_output_error += 1
    #             push!(input_failed_error, (i, output_value, (abs(output_value*i - output_values[output_value]), output_errors[output_value])))
    #         end
    #     end
    # end
    # if count_output_error != 0
    #     verbose && println("Inputs that fail: $input_failed_error")
    #     open("results_failed$(benchmark_naming).txt", "a") do file
    #         write(file, "$(benchmark_info[1]) -- failed")
    #         write(file, "\n")
    #     end
    # else
    #     open("results_failed$(benchmark_naming).txt", "a") do file
    #         write(file, "$(benchmark_info[1]) -- succeed")
    #         write(file, "\n")
    #     end
    # end
    println(write_addergraph(ag))
    println(write_addergraph_truncations(ag))

    open("results$(benchmark_naming).txt", "a") do file
        if isvalid(ag)
            if isempty(oddabsC)
                write(file, "$(benchmark_info[1]) -- 0")
            elseif has_values(model)
                if model[:valid_objective_value] != 0
                    write(file, "$(benchmark_info[1]) -- $(round(Int, model[:valid_objective_value]))")
                    if termination_status(model) != MOI.OPTIMAL
                        write(file, "*")
                        if round(Int, objective_value(model; result = 1)) != round(Int, model[:valid_objective_value])
                            write(file, "*")
                        end
                        write(file, " -- $(length(ag))")
                    else
                        if round(Int, objective_value(model; result = 1)) != round(Int, model[:valid_objective_value])
                            write(file, "**")
                        end
                        write(file, " -- $(length(ag))")
                        write(file, " -- $(solving_time)")
                    end
                else
                    write(file, "$(benchmark_info[1]) -- error")
                end
            else
                write(file, "$(benchmark_info[1]) -- TO")
            end
        else
            write(file, "$(benchmark_info[1]) -- error")
        end
        write(file, "\n")
    end
    return ag
end
