#include("../utils.jl")
#include("mcm/ilp1.jl")

"""
using JuMP; using AdderGraphs; using CPLEX; using jMCM;
include("/home/remi/Projects/Dev/WIP/jMCMTruncate/src/ilp1.jl")
include("/home/remi/Projects/Dev/WIP/jMCMTruncate/src/mcm.jl")
include("/home/remi/Projects/Dev/WIP/jAGTruncate/src/extract_vars.jl")
include("/home/remi/Projects/Dev/WIP/jAGTruncate/src/truncate.jl")
ag = truncate!(mcm(Model(CPLEX.Optimizer), [2458, 3907, 3206], wordlength=12, verbose=true, nb_adders_lb=7), Model(CPLEX.Optimizer), msb_in=12);

model = Model(Gurobi.Optimizer);

mcm_truncate(model, [2458, 3907, 3206], wordlength=12, verbose=true, nb_adders_lb=7, msb_in=12, addergraph_warmstart=ag);

mcm(model, [7], wordlength=6, verbose=true)
mcm(model, [7, 19, 31], wordlength=6, verbose=true)
mcm(model, [2458, 3907, 3206], wordlength=12, verbose=true)

using JuMP; using AdderGraphs; using Gurobi;
include("/home/remi/Projects/Dev/WIP/jMCMTruncate/src/ilp1.jl")
include("/home/remi/Projects/Dev/WIP/jMCMTruncate/src/mcm.jl")
model = Model(Gurobi.Optimizer);
ag = mcm_truncate(model, [2458, 3907, 3206], wordlength=12, verbose=true, nb_adders_lb=7, msb_in=12);
"""


"""
    mcm(model::Model,
        C::Vector{Int},
        ;wordlength::Int = 0,
        use_nlp::Bool = false,
        ilp::Int = 1, # 1 : ILP1 -- 2 : ILP2
        use_big_m::Bool = true, # Only for ILP1
        adder_depth_max::Int = 0, # 0 to deactivate this bound
        verbose::Bool = false,
    )

Add to `model`, an empty model with a solver attached, an mcm modelization.
The model is then optimized and the solution is transposed into an AdderGraph.
"""
function mcm_truncate(model::Model,
                      C::Vector{Int},
                      ;wordlength::Int = 0,
                      use_nlp::Bool = false,
                      ilp::Int = 1, # 1 : ILP1 -- 2 : ILP2
                      use_big_m::Bool = true, # Only for ILP1
                      adder_depth_max::Int = 0, # 0 to deactivate this bound
                      lsb_in::Int = 0,
                      msb_in::Int,
                      input_error::Float64 = 0.0,
                      output_errors_dict::Dict{Int, Float64}=Dict{Int, Float64}(),
                      output_errors::Vector{Float64}=Vector{Float64}(),
                      output_error::Float64=0.0,
                      verbose::Bool = false,
                      kwargs...
    )
    if isempty(C)
        return AdderGraph()
    end
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    if isempty(oddabsC)
        return AdderGraph(C)
    end
    if isempty(output_errors_dict) && isempty(output_errors)
        output_errors_dict = Dict{Int, Float64}([output_value => output_error for output_value in C])
    elseif isempty(output_errors_dict)
        output_errors_dict = Dict{Int, Float64}([C[i] => output_errors[i] for i in 1:length(C)])
    end
    @assert length(output_errors_dict) == length(unique(C))
    output_errors_odd_dict = Dict{Int, Float64}([odd(abs(C[i])) => Inf for i in 1:length(C)])
    for i in 1:length(C)
        if C[i] == 0
            continue
        end
        output_errors_odd_dict[odd(abs(C[i]))] = min(output_errors_odd_dict[odd(abs(C[i]))], output_errors_dict[C[i]]/(div(abs(C[i]), odd(abs(C[i])))))
    end
    output_errors = Vector{Int}([round(Int, output_errors_odd_dict[output_value]*2.0^(-lsb_in), RoundDown) for output_value in oddabsC])
    if wordlength == 0
        wordlength = maximum(get_min_wordlength.(oddabsC))
    end
    verbose && println("Coefficients wordlength: $(wordlength)")
    if (1 << wordlength) - 1 < maximum(oddabsC)
        return AdderGraph()
    end
    !verbose && set_silent(model)

    addergraph = AdderGraph()
    model_mcm_forumlation! = model_mcm_formulation_1_odd_big_m!

    if use_big_m
        model_mcm_forumlation! = model_mcm_formulation_1_odd_big_m!
    else
        error("Not implemented yet.")
        if adder_depth_max == 0
            model_mcm_forumlation! = model_mcm_formulation_1_odd_ind!
        end
    end
    optimize_increment!(model, model_mcm_forumlation!, oddabsC, wordlength, (-wordlength,wordlength),
        adder_depth_max=adder_depth_max,
        msb_in=msb_in-lsb_in,
        input_error=round(Int, input_error*2.0^(-lsb_in)),
        output_errors=output_errors,
        verbose=verbose; kwargs...)

    current_result = 1
    not_valid = true
    model[:valid_objective_value] = 0
    while not_valid && has_values(model; result=current_result)
        addergraph = AdderGraph(C)
        for i in 1:model[:NA][current_result]
            node_shift = 0
            for s in -wordlength:0
                if round(Int, value(model[:Psias][i,s]; result = current_result)) == 1
                    node_shift = s
                    break
                end
            end
            input_shift = 0
            for s in 0:wordlength
                if round(Int, value(model[:phias][i,s]; result = current_result)) == 1
                    input_shift = s
                    break
                end
            end
            subtraction = [value(model[:cai_left_shsg][i]; result = current_result) < 0, value(model[:cai_right_sg][i]; result = current_result) < 0]
            if !isnothing(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,1]; result = current_result)))) && !isnothing(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,2]; result = current_result))))
                push_node!(addergraph,
                    AdderNode(round(Int, value(model[:ca][i]; result = current_result)),
                        [InputEdge(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,1]; result = current_result))), input_shift+node_shift, subtraction[1], round(Int, value(model[:truncateleft][i]; result = current_result))),
                        InputEdge(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,2]; result = current_result))), node_shift, subtraction[2], round(Int, value(model[:truncateright][i]; result = current_result)))],
                    )
                )
            else
                push_node!(addergraph,
                    AdderNode(round(Int, value(model[:ca][i]; result = current_result)),
                        [InputEdge(get_origin(addergraph), input_shift+node_shift, subtraction[1], round(Int, value(model[:truncateleft][i]; result = current_result))),
                        InputEdge(get_origin(addergraph), node_shift, subtraction[2], round(Int, value(model[:truncateright][i]; result = current_result)))],
                    )
                )
            end
        end
        not_valid = !isvalid(addergraph)
        if not_valid
            addergraph = AdderGraph()
        else
            model[:valid_objective_value] = objective_value(model; result = current_result)
        end
        current_result = current_result + 1
    end

    return addergraph
end
