module jAGTruncate

using JuMP
using AdderGraphs

include("extract_vars.jl")
include("truncate.jl")

export truncate!
export truncate_model!
export get_msb_var_symbol
export get_msb_var
export get_FA_var_symbol
export get_FA_var
export get_g_var_symbol
export get_g_var
export get_internal_error_var_symbol
export get_internal_error_var
export get_internal_error_input_left_var_symbol
export get_internal_error_input_left_var
export get_internal_error_input_right_var_symbol
export get_internal_error_input_right_var
export get_truncateleft_var_symbol
export get_truncateleft_var
export get_truncateright_var_symbol
export get_truncateright_var
export get_truncateleft_bit_var_symbol
export get_truncateleft_bit_var
export get_truncateright_bit_var_symbol
export get_truncateright_bit_var

end # module
