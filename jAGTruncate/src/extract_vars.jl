function get_msb_var_symbol(model)
    return :msb_a
end

function get_msb_var(model)
    return model[get_msb_var_symbol(model)]
end

function get_FA_var_symbol(model)
    return :FA_a
end

function get_FA_var(model)
    return model[get_FA_var_symbol(model)]
end

function get_g_var_symbol(model)
    return :g_a
end

function get_g_var(model)
    return model[get_g_var_symbol(model)]
end

function get_internal_error_var_symbol(model)
    return :internal_error
end

function get_internal_error_var(model)
    return model[get_internal_error_var_symbol(model)]
end

function get_internal_error_input_left_var_symbol(model)
    return :internal_error_input_left
end

function get_internal_error_input_left_var(model)
    return model[get_internal_error_input_left_var_symbol(model)]
end

function get_internal_error_input_right_var_symbol(model)
    return :internal_error_input_right
end

function get_internal_error_input_right_var(model)
    return model[get_internal_error_input_right_var_symbol(model)]
end

function get_truncateleft_var_symbol(model)
    return :truncateleft
end

function get_truncateleft_var(model)
    return model[get_truncateleft_var_symbol(model)]
end

function get_truncateright_var_symbol(model)
    return :truncateright
end

function get_truncateright_var(model)
    return model[get_truncateright_var_symbol(model)]
end

function get_truncateleft_bit_var_symbol(model)
    return :truncateleft_bit
end

function get_truncateleft_bit_var(model)
    return model[get_truncateleft_bit_var_symbol(model)]
end

function get_truncateright_bit_var_symbol(model)
    return :truncateright_bit
end

function get_truncateright_bit_var(model)
    return model[get_truncateright_bit_var_symbol(model)]
end
