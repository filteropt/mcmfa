"""
Example from "Table-Based versus Shift-And-Add Constant Multipliers for FPGAs":
using JuMP; using AdderGraphs; using CPLEX;
Astr = "{{'O',[3206],6,[1603],5,1},{'O',[3907],6,[3907],4,0},{'O',[2458],6,[1229],3,1},{'A',[5],1,[1],0,2,[1],0,0},{'A',[9],1,[1],0,3,[1],0,0},{'A',[55],2,[1],0,6,[-9],1,0},{'A',[321],2,[5],1,6,[1],0,0},{'A',[1229],3,[321],2,2,[-55],2,0},{'A',[3907],4,[321],2,4,[-1229],3,0},{'A',[1603],5,[-9],1,8,[3907],4,0}}";
A = read_addergraph(Astr);
model = Model(CPLEX.Optimizer);
include("/home/remi/projects/Dev/WIP/jAGTruncate/src/extract_vars.jl")
include("/home/remi/projects/Dev/WIP/jAGTruncate/src/truncate.jl")
truncate!(A, model, msb_in=20, lsb_in=0, output_errors=Vector{Float64}([2^10,2^10,2^10]), input_error=0.0)
sum(value.(model[:g_a])) # 46?
"""


function truncate_model!(model::Model, addergraph::AdderGraph;
                         truncate_max::Int=-1,
                         msb_in::Int,
                         output_errors::Union{Vector{Int}, Vector{VariableRef}},
                         input_error::Union{Int, VariableRef}=0,)
    NA = length(addergraph)
    addernodes = get_nodes(addergraph)
    output_values = get_outputs(addergraph)
    addernodes_value_to_index = Dict{Int, Int}([get_value(addernodes[i]) => i for i in 1:length(addernodes)])
    addernodes_value_to_index[1] = 0
    max_msb = msb_in+round(Int, log2(maximum([get_value(addernodes[i]) for i in 1:length(addernodes)])), RoundUp)+1

    if truncate_max < 0
        if typeof(output_errors) == Vector{VariableRef}
            truncate_max = max_msb
        end
        truncate_max = min(round(Int, log2(maximum(output_errors)+1), RoundUp), max_msb)
    end
    @variable(model, 0 <= FA_a[1:NA] <= max_msb+1, Int)
    @variable(model, 0 <= g_a[1:NA] <= max_msb, Int)
    @variable(model, 0 <= msb_a[1:NA] <= max_msb, Int)
    @variable(model, psi_a[1:NA], Bin)
    @variable(model, internal_error[0:NA] >= 0, Int)
    @variable(model, internal_error_input_left[1:NA] >= 0, Int)
    @variable(model, internal_error_input_right[1:NA] >= 0, Int)
    @variable(model, truncateleft[1:NA] >= 0, Int)
    @variable(model, truncateright[1:NA] >= 0, Int)
    @variable(model, truncateleft_bit[1:NA, 0:truncate_max], Bin)
    @variable(model, truncateright_bit[1:NA, 0:truncate_max], Bin)

    @constraint(model, [a in 1:NA], FA_a[a] == msb_a[a] - g_a[a] + psi_a[a])
    @constraint(model, [a in 1:NA], g_a[a] <= msb_a[a])

    @constraint(model, [a in 1:NA], sum(truncateleft_bit[a, w] for w in 0:truncate_max) == 1)
    @constraint(model, [a in 1:NA], sum(truncateright_bit[a, w] for w in 0:truncate_max) == 1)

    @constraint(model, [a in 1:NA], sum(w*truncateleft_bit[a, w] for w in 0:truncate_max) == truncateleft[a])
    @constraint(model, [a in 1:NA], sum(w*truncateright_bit[a, w] for w in 0:truncate_max) == truncateright[a])

    @constraint(model, internal_error[0] == input_error)

    for a in 1:NA
        left_input, right_input = addernodes_value_to_index[get_input_addernode_values(addernodes[a])[1]], addernodes_value_to_index[get_input_addernode_values(addernodes[a])[2]]
        left_shift, right_shift = get_input_shifts(addernodes[a])
        # if left_input != 0
        #     @constraint(model, FA_a[a] >= msb_a[left_input]+max(left_shift,0) - g_a[a]+1)
        # else
        #     @constraint(model, FA_a[a] >= msb_in+max(left_shift,0) - g_a[a]+1)
        # end
        # if right_input != 0
        #     @constraint(model, FA_a[a] >= msb_a[right_input]+max(right_shift,0) - g_a[a]+1)
        # else
        #     @constraint(model, FA_a[a] >= msb_in+max(right_shift,0) - g_a[a]+1)
        # end
        if left_input != 0
            @constraint(model, msb_a[left_input] + left_shift + 1 <= msb_a[a] + psi_a[a]*(2*max_msb))
        else
            @constraint(model, msb_in + left_shift + 1 <= msb_a[a] + psi_a[a]*(2*max_msb))
        end
        if right_input != 0
            @constraint(model, msb_a[right_input] + right_shift + 1 <= msb_a[a] + psi_a[a]*(2*max_msb))
        else
            @constraint(model, msb_in + right_shift + 1 <= msb_a[a] + psi_a[a]*(2*max_msb))
        end
        if left_shift >= 0 && right_shift >= 0
            @constraint(model, internal_error_input_left[a] >= 2^left_shift*internal_error[left_input])
            @constraint(model, internal_error_input_right[a] >= 2^right_shift*internal_error[right_input])
            @constraint(model, internal_error_input_left[a] >= sum((2^(w+left_shift)-1)*truncateleft_bit[a, w] for w in 1:truncate_max))
            @constraint(model, internal_error_input_right[a] >= sum((2^(w+right_shift)-1)*truncateright_bit[a, w] for w in 1:truncate_max))
            @constraint(model, internal_error_input_left[a] >= 2^left_shift*internal_error[left_input] + sum((2.0^(w+left_shift)-1)*truncateleft_bit[a, w] for w in 1:truncate_max))
            @constraint(model, internal_error_input_right[a] >= 2^right_shift*internal_error[right_input] + sum((2^(w+right_shift)-1)*truncateright_bit[a, w] for w in 1:truncate_max))
        elseif left_shift == right_shift
            @constraint(model, 2^(-left_shift)*internal_error_input_left[a] >= internal_error[left_input])
            @constraint(model, 2^(-right_shift)*internal_error_input_right[a] >= internal_error[right_input])
            @constraint(model, internal_error_input_left[a] >= sum((2.0^(w+left_shift)-1)*truncateleft_bit[a, w] for w in 1:truncate_max))
            @constraint(model, internal_error_input_right[a] >= sum((2.0^(w+right_shift)-1)*truncateright_bit[a, w] for w in 1:truncate_max))
            @constraint(model, 2^(-left_shift)*internal_error_input_left[a] >= internal_error[left_input] + sum((2.0^(w+left_shift)-1)*truncateleft_bit[a, w] for w in 1:truncate_max))
            @constraint(model, 2^(-right_shift)*internal_error_input_right[a] >= internal_error[right_input] + sum((2.0^(w+right_shift)-1)*truncateright_bit[a, w] for w in 1:truncate_max))
        else
            @error "Case not handled yet"
        end

        @constraint(model, internal_error[a] >= internal_error_input_left[a]+internal_error_input_right[a])
    end

    @constraint(model, [j in 1:length(output_values); output_values[j] != 0], 2^(round(Int, log2(div(abs(output_values[j]), odd(abs(output_values[j]))))))*internal_error[addernodes_value_to_index[odd(abs(output_values[j]))]] <= output_errors[j])

    for a in 1:NA
        left_input, right_input = addernodes_value_to_index[get_input_addernode_values(addernodes[a])[1]], addernodes_value_to_index[get_input_addernode_values(addernodes[a])[2]]
        left_shift, right_shift = get_input_shifts(addernodes[a])
        left_negative, right_negative = are_negative_inputs(addernodes[a])
        if left_negative == right_negative && (left_shift >= 0 || right_shift >= 0)
            tmp = @variable(model, binary=true)
            @constraint(model, g_a[a] <= (truncateleft[a]+left_shift)+tmp*max_msb)
            @constraint(model, g_a[a] <= (truncateright[a]+right_shift)+(1-tmp)*max_msb)
        elseif left_shift >= 0 && left_negative
            @constraint(model, g_a[a] == truncateleft[a]+left_shift)
        elseif right_shift >= 0 && right_negative
            @constraint(model, g_a[a] == truncateright[a]+right_shift)
        else
            @constraint(model, g_a[a] == 0)
        end
    end

    @constraint(model, [a in 1:NA], msb_a[a] == round(Int, log2((2^(msb_in)-1)*get_value(addernodes[a])), RoundUp))

    @objective(model, Min, sum(FA_a))

    return model
end

function truncate!(addergraph::AdderGraph, model::Model;
                   msb_in::Int, lsb_in::Int=0,
                   output_errors_dict::Dict{Int, Float64}=Dict{Int, Float64}(),
                   output_errors::Vector{Float64}=Vector{Float64}(),
                   output_error::Float64=0.0,
                   input_error::Float64=0.0)
    output_values = get_outputs(addergraph)
    if isempty(output_errors_dict) && isempty(output_errors)
        output_errors_dict = Dict{Int, Float64}([output_value => output_error for output_value in output_values])
    elseif isempty(output_errors_dict)
        output_errors_dict = Dict{Int, Float64}([output_values[i] => output_errors[i] for i in 1:length(output_values)])
    end
    @assert length(output_errors_dict) == length(output_values)
    output_errors = Vector{Int}([round(Int, output_errors_dict[output_value]*2.0^(-lsb_in), RoundDown) for output_value in output_values])
    @assert minimum(output_errors) >= 0.0
    truncate_model!(model, addergraph,
        msb_in=msb_in-lsb_in,
        input_error=round(Int, input_error*2.0^(-lsb_in)),
        output_errors=output_errors
    )
    optimize!(model)

    addernodes = get_nodes(addergraph)
    for i in 1:length(addernodes)
        left_shift, right_shift = get_input_shifts(addernodes[i])
        set_truncations!(addernodes[i],
            Vector{Int}([round(Int, value(get_truncateleft_var(model)[i]))
            round(Int, value(get_truncateright_var(model)[i]))]))
        set_adder_error!(addernodes[i], round(Int, value(get_internal_error_var(model)[i]))*(2.0^(lsb_in)))
        set_nb_full_adders!(addernodes[i], round(Int, value(get_FA_var(model)[i])))
    end
    set_lsb_in!(addergraph, lsb_in)
    set_msb_in!(addergraph, msb_in)
    set_error_in!(addergraph, input_error)
    set_data_paths!(addergraph)
    set_errors_computed!(addergraph)
    set_full_adders_computed!(addergraph)

    return addergraph
end
