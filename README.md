# MCM Full Adders

This tool solves the MCM problem w.r.t the full adder metric.

Its inputs are the target coefficients and an acceptable output error. See `jMCMTruncate/benchmarks/benchmarks.jl` for usage.

### Prerequisites

* `julia 1.6.1`
* `JuMP`
* `Gurobi` or `CPLEX`
* [`AdderGraphs`](https://gitlab.univ-nantes.fr/volkova-a/jiir2hw/-/tree/master/src/AdderGraphs)

### Archive for ISCAS2022

Benchmarks were run with `julia 1.6.1`. Gurobi must be installed along with the julia packages `Gurobi` and `JuMP`.

Run scripts `jMCMTruncate/benchmarks/benchmarks.sh` for our approach and `jMCMTruncate/benchmarks/benchmarks_mcm.sh` for the state-of-the-art.

Obtained results are in `jMCMTruncate/benchmarks/results/`.

If any question, please contact first author.
