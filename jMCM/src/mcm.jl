#using GLPK

#include("../utils.jl")
#include("mcm/ilp1.jl")
#include("mcm/ilp2.jl")
#include("mcm/nlp.jl")

"""
    mcm(model::Model,
        C::Vector{Int},
        ;wordlength::Int = 0,
        use_nlp::Bool = false,
        ilp::Int = 1, # 1 : ILP1 -- 2 : ILP2
        use_big_m::Bool = true, # Only for ILP1
        adder_depth_max::Int = 0, # 0 to deactivate this bound
        verbose::Bool = false,
    )

Add to `model`, an empty model with a solver attached, an mcm modelization.
The model is then optimized and the solution is transposed into an AdderGraph.
"""
function mcm(model::Model,
             C::Vector{Int},
             ;wordlength::Int = 0,
             use_big_m::Bool = true, # Only for ILP1
             #avoid_internal_shifts::Bool = false, # Only implemented for ILP1 yet
             #adder_depth_max::Int = 0, # 0 to deactivate this bound
             #nb_adders_lb::Int = 0, # 0: automatically compute
             verbose::Bool = false,
             kwargs...
    )
    if isempty(C)
        return AdderGraph()
    end
    oddabsC = filter!(x -> x > 1, unique!(odd.(abs.(C))))
    if isempty(oddabsC)
        return AdderGraph(C)
    end
    if wordlength == 0
        wordlength = maximum(get_min_wordlength.(oddabsC))
    end
    verbose && println("Coefficients wordlength: $(wordlength)")
    if (1 << wordlength) - 1 < maximum(oddabsC)
        return AdderGraph()
    end
    !verbose && set_silent(model)

    addergraph = AdderGraph()
    model_mcm_forumlation! = model_mcm_formulation_1_odd_big_m!

    if !use_big_m
        error("Not implemented yet.")
        model_mcm_forumlation! = model_mcm_formulation_1_odd_ind!
    end
    optimize_increment!(model, model_mcm_forumlation!, oddabsC, wordlength,
        (-wordlength,wordlength), verbose=verbose; kwargs...)

    current_result = 1
    not_valid = true
    model[:valid_objective_value] = 0
    while not_valid && has_values(model; result=current_result)
        addergraph = AdderGraph(C)
        for i in 1:model[:NA][current_result]
            node_shift = 0
            for s in -wordlength:0
                if round(Int, value(model[:Psias][i,s]; result = current_result)) == 1
                    node_shift = s
                    break
                end
            end
            input_shift = 0
            for s in 0:wordlength
                if round(Int, value(model[:phias][i,s]; result = current_result)) == 1
                    input_shift = s
                    break
                end
            end
            subtraction = [value(model[:cai_left_shsg][i]; result = current_result) < 0, value(model[:cai_right_sg][i]; result = current_result) < 0]
            if !isnothing(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,1]; result = current_result)))) && !isnothing(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,2]; result = current_result))))
                push_node!(addergraph,
                    AdderNode(round(Int, value(model[:ca][i]; result = current_result)),
                        [InputEdge(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,1]; result = current_result))), input_shift+node_shift, subtraction[1]),
                        InputEdge(get_addernode_by_value(addergraph, round(Int, value(model[:cai][i,2]; result = current_result))), node_shift, subtraction[2])])
                )
            else
                push_node!(addergraph,
                    AdderNode(round(Int, value(model[:ca][i]; result = current_result)),
                        [InputEdge(get_origin(addergraph), input_shift+node_shift, subtraction[1]),
                        InputEdge(get_origin(addergraph), node_shift, subtraction[2])])
                )
            end
        end
        not_valid = !isvalid(addergraph)
        verbose && println(write_addergraph(addergraph))
        verbose && println("Valid: $(!not_valid)")
        if not_valid
            addergraph = AdderGraph()
        else
            model[:valid_objective_value] = objective_value(model; result = current_result)
        end
        current_result = current_result + 1
    end

    return addergraph
end
