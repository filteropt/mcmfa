#using JuMP

"""
    model_mcm_formulation_1_odd_big_m!(model::Model, C::Vector{Int},
                                       wordlength::Int, S::Tuple{Int, Int},
                                       NA::Int, adder_depth_max::Int)::Model

Formulation 1 from "M. Kumm -- Optimal Constant Multiplication Using Integer
Linear Programming" with small modifications: no left shifts and only positive
right shifts
"""
function model_mcm_formulation_1_odd_big_m!(model::Model, C::Vector{Int},
                                            wordlength::Int, S::Tuple{Int, Int},
                                            NA::Int;
                                            use_all_adders::Bool=true,
                                            adder_depth_max::Int=0,
                                            minimize_adderdepth::Bool=false,
                                            avoid_internal_shifts::Bool=false,
                                            addergraph_warmstart::AdderGraph=AdderGraph(),
                                            verbose::Bool=false,
                                            use_symmetry_breaking_cst::Bool=true,
                                            known_min_NA::Int=0
    )::Model
    use_warmstart = false
    addernodes = Vector{AdderNode}()
    output_values = Vector{Int}()
    addernodes_value_to_index = Dict{Int, Int}()
    nb_var_warmstart = 0
    if !isempty(addergraph_warmstart)
        use_warmstart = true
        addernodes = get_nodes(addergraph_warmstart)
        for addernode in addernodes
            sort!(addernode.inputs, by=x->x.shift, rev=true)
        end
        sort!(addernodes, by=x->(
        (get_value(x) in C && (2^(round(Int, log2(get_value(x)+1))) == get_value(x)+1 || 2^(round(Int, log2(get_value(x)-1))) == get_value(x)-1)) ? 0 : 1,
        get_depth(x), get_value(x)))
        # println(get_value.(addernodes))
        # println(get_depth.(addernodes))

        # for i in 1:length(addernodes)
        #     addernode = addernodes[i]
        #     j = i
        #     while j >= 2 && get_value(addernode) < get_value(addernodes[j-1]) && !(get_value(addernodes[j-1]) in get_input_addernode_values(addernode))
        #         addernodes[j], addernodes[j-1] = addernodes[j-1], addernodes[j]
        #         j = j - 1
        #     end
        # end
        output_values = get_outputs(addergraph_warmstart)
        addernodes_value_to_index = Dict{Int, Int}([get_value(addernodes[i]) => i for i in 1:length(addernodes)])
        addernodes_value_to_index[1] = 0
        nb_var_warmstart = min(NA, length(addernodes))
    end
    Smin, Smax = S
    NO = length(C)
    maximum_target = maximum(C)
    maximum_value = 2^wordlength
    known_min_NA = min(NA, max(get_min_number_of_adders(C), known_min_NA))
    if use_all_adders
        known_min_NA = NA
    end
    verbose && println("\tBounds on the number of adder: $(known_min_NA)--$(NA)")
    maximum_target = maximum(C)
    maximum_value = 2^wordlength

    @variable(model, 1 <= ca[0:NA] <= maximum_value-1, Int)
    @constraint(model, [a in 1:known_min_NA], ca[a] >= 3)
    @variable(model, 1 <= ca_no_shift[1:NA] <= maximum_value*2, Int)
    @variable(model, 1 <= cai[1:NA, 1:2] <= maximum_value-1, Int)
    @variable(model, 1 <= cai_left_sh[1:NA] <= maximum_value*2, Int)
    @variable(model, -2*maximum_value <= cai_left_shsg[1:NA] <= maximum_value*2, Int)
    @variable(model, -2*maximum_value <= cai_right_sg[1:NA] <= maximum_value*2, Int)

    @variable(model, Phiai[1:NA, 1:2], Bin)
    @variable(model, caik[a in 1:NA, 1:2, 0:(a-1)], Bin)
    @variable(model, phias[1:NA, 0:Smax], Bin)
    @variable(model, oaj[1:NA, 1:NO], Bin)

    if use_symmetry_breaking_cst || minimize_adderdepth || adder_depth_max != 0
        # Add adder depth
        @variable(model, 0 <= ada[0:NA] <= NA)
        @variable(model, 0 <= adai[1:NA, i in 1:2] <= NA)
        fix(ada[0], 0, force=true)
        fix(ada[1], 1, force=true)
        @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], adai[a,i] <= ada[k] + (1-caik[a,i,k])*NA)
        @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], adai[a,i] >= ada[k] - (1-caik[a,i,k])*NA)
        @constraint(model, [a in 1:NA, i in 1:2], ada[a] >= adai[a,i]+1)
        @variable(model, ada_case[1:NA], Bin)
        @constraint(model, [a in 1:NA], ada[a] <= adai[a,1]+1 + NA*(1-ada_case[a]))
        @constraint(model, [a in 1:NA], ada[a] <= adai[a,2]+1 + NA*(ada_case[a]))
        if minimize_adderdepth || adder_depth_max != 0
            @variable(model, 1 <= max_ad <= NA, Int)
            @constraint(model, [a in 1:NA], max_ad >= ada[a])
            if adder_depth_max >= 1
                @constraint(model, max_ad <= adder_depth_max)
            end
        end
    end

    @variable(model, 0 <= force_odd[1:NA] <= maximum_value, Int)
    @variable(model, Psias[1:NA, Smin:0], Bin)

    if use_warmstart
        set_start_value(ca[0], 1)
        if use_symmetry_breaking_cst || minimize_adderdepth || adder_depth_max != 0
            set_start_value(ada[0], 0)
        end
        current_adder_depth_max_value = 0
        for a in 1:nb_var_warmstart
            left_input, right_input = addernodes_value_to_index[get_input_addernode_values(addernodes[a])[1]], addernodes_value_to_index[get_input_addernode_values(addernodes[a])[2]]
            left_input_value = 1
            if left_input != 0
                left_input_value = get_value(addernodes[left_input])
            end
            right_input_value = 1
            if right_input != 0
                right_input_value = get_value(addernodes[right_input])
            end
            left_shift, right_shift = get_input_shifts(addernodes[a])
            left_negative, right_negative = are_negative_inputs(addernodes[a])
            set_start_value(ca[a], get_value(addernodes[a]))
            set_start_value(ca_no_shift[a], get_value(addernodes[a])*(2^max(-right_shift, 0)))
            set_start_value(force_odd[a], div(get_value(addernodes[a]), 2))
            set_start_value(cai[a, 1], left_input_value)
            set_start_value(cai[a, 2], right_input_value)
            set_start_value(cai_left_sh[a], left_input_value*(2^max(0, left_shift)))
            if left_negative
                set_start_value(cai_left_shsg[a], -left_input_value*(2^max(0, left_shift)))
            else
                set_start_value(cai_left_shsg[a], left_input_value*(2^max(0, left_shift)))
            end
            if right_negative
                set_start_value(cai_right_sg[a], -right_input_value)
            else
                set_start_value(cai_right_sg[a], right_input_value)
            end
            for k in 0:(a-1)
                set_start_value(caik[a, 1, k], 0)
                set_start_value(caik[a, 2, k], 0)
            end
            set_start_value(caik[a, 1, left_input], 1)
            set_start_value(caik[a, 2, right_input], 1)
            set_start_value(Phiai[a, 1], left_negative)
            set_start_value(Phiai[a, 2], right_negative)
            set_start_value.(Psias[a, :], 0)
            set_start_value.(phias[a, :], 0)
            if right_shift >= Smin && right_shift <= 0
                set_start_value(Psias[a, right_shift], 1)
            end
            if left_shift >= 0 && left_shift <= Smax
                set_start_value(phias[a, left_shift], 1)
            end
            set_start_value.(oaj[a, :], 0)
            if get_value(addernodes[a]) in C
                set_start_value(oaj[a, findfirst(isequal(get_value(addernodes[a])), C)], 1)
            end
            if use_symmetry_breaking_cst || minimize_adderdepth || adder_depth_max != 0
                set_start_value(ada[a], get_depth(addernodes[a]))
                left_ad = 0
                if left_input != 0
                    left_ad = get_depth(addernodes[left_input])
                end
                right_ad = 0
                if right_input != 0
                    right_ad = get_depth(addernodes[right_input])
                end
                set_start_value(adai[a, 1], left_ad)
                set_start_value(adai[a, 2], right_ad)
                set_start_value(ada_case[a], get_depth(addernodes[a]) == right_ad+1 ? 0 : 1)
                current_adder_depth_max_value = max(current_adder_depth_max_value, get_depth(addernodes[a]))
            end
        end
        # println(start_value.(ca))
        # println(start_value.(ada))
        # println(start_value.(adai[:,1]))
        # println(start_value.(adai[:,2]))
        if minimize_adderdepth || adder_depth_max != 0
            set_start_value(max_ad, current_adder_depth_max_value)
        end
    end

    # C1
    fix(ca[0], 1, force=true)
    # C2 - Modified
    @constraint(model, [a in 1:NA], ca_no_shift[a] == cai_left_shsg[a] + cai_right_sg[a])
    # C3a - C3b
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], cai[a,i] <= ca[k] + (1-caik[a,i,k])*maximum_value)
    @constraint(model, [a in 1:NA, i in 1:2, k in 0:(a-1)], cai[a,i] >= ca[k] - (1-caik[a,i,k])*maximum_value)
    @constraint(model, [a in 1:NA, i in 1:2], sum(caik[a,i,k] for k in 0:(a-1)) == 1)
    # C4a - C4b - Modified
    @constraint(model, [a in 1:NA, s in 0:Smax], cai_left_sh[a] <= 2^s*cai[a,1] + (1-phias[a,s])*2*maximum_value)
    @constraint(model, [a in 1:NA, s in 0:Smax], cai_left_sh[a] >= 2^s*cai[a,1] - (1-phias[a,s])*(2*maximum_value*(2^s)))
    @constraint(model, [a in 1:NA], sum(phias[a,s] for s in 0:Smax) == 1)
    # C5a - C5b - C5c - Modified
    @constraint(model, [a in 1:NA], cai_left_shsg[a] <= cai_left_sh[a] + Phiai[a,1]*2*maximum_value)
    @constraint(model, [a in 1:NA], cai_left_shsg[a] >= cai_left_sh[a] - Phiai[a,1]*(4*maximum_value))
    @constraint(model, [a in 1:NA], cai_left_shsg[a] <= -cai_left_sh[a] + (1-Phiai[a,1])*(4*maximum_value))
    @constraint(model, [a in 1:NA], cai_left_shsg[a] >= -cai_left_sh[a] - (1-Phiai[a,1])*2*maximum_value)
    @constraint(model, [a in 1:NA], cai_right_sg[a] <= cai[a,2] + Phiai[a,2]*maximum_value)
    @constraint(model, [a in 1:NA], cai_right_sg[a] >= cai[a,2] - Phiai[a,2]*(2*maximum_value))
    @constraint(model, [a in 1:NA], cai_right_sg[a] <= -cai[a,2] + (1-Phiai[a,2])*(2*maximum_value))
    @constraint(model, [a in 1:NA], cai_right_sg[a] >= -cai[a,2] - (1-Phiai[a,2])*maximum_value)
    @constraint(model, [a in 1:NA], Phiai[a,1] + Phiai[a,2] <= 1)
    # C6a - C6b
    @constraint(model, [a in 1:NA, j in 1:NO], ca[a] <= C[j] + (1-oaj[a,j])*maximum_value)
    @constraint(model, [a in 1:NA, j in 1:NO], ca[a] >= C[j] - (1-oaj[a,j])*maximum_target)
    @constraint(model, [j in 1:NO], sum(oaj[a,j] for a in 1:NA) == 1)

    # Odd
    @constraint(model, [a in 1:NA], ca[a] == 2*force_odd[a]+1)
    @constraint(model, [a in 1:NA, s in Smin:0], ca_no_shift[a] >= 2^(-s)*ca[a] + (Psias[a,s] - 1)*(maximum_value*(2^(-s))))
    @constraint(model, [a in 1:NA, s in Smin:0], ca_no_shift[a] <= 2^(-s)*ca[a] + (1 - Psias[a,s])*(maximum_value*(2^(-s))))
    @constraint(model, [a in 1:NA], sum(Psias[a,s] for s in Smin:0) == 1)
    @constraint(model, [a in 1:NA], phias[a,0] == sum(Psias[a,s] for s in Smin:-1))

    # Fix some variables
    fix(caik[1,1,0], 1, force=true)
    fix(caik[1,2,0], 1, force=true)
    fix(cai[1,1], 1, force=true)
    fix(cai[1,2], 1, force=true)
    start_sym_break = 1
    for output_value in C
        if 2^(round(Int, log2(output_value+1))) == output_value+1 || 2^(round(Int, log2(output_value-1))) == output_value-1
            println("Fix $output_value")
            fix(ca[start_sym_break], output_value, force=true)
            start_sym_break += 1
        end
    end

    if known_min_NA < NA
        @variable(model, used_adder[(known_min_NA+1):NA], Bin)
        if use_warmstart
            for a in (known_min_NA+1):NA
                set_start_value(used_adder[a], 0)
            end
            for a in (known_min_NA+1):nb_var_warmstart
                set_start_value(used_adder[a], 1)
            end
        end
        if (known_min_NA+2) <= NA
            @constraint(model, [a in (known_min_NA+2):NA], used_adder[a] <= used_adder[a-1])
        end
        @constraint(model, [a in (known_min_NA+1):NA], ca[a] <= used_adder[a]*maximum_value + 1)
        @constraint(model, [a in (known_min_NA+1):NA], ca[a] >= 3*used_adder[a])
        @constraint(model, [a in (known_min_NA+1):NA, i in 1:2], caik[a,i,0] >= 1-used_adder[a])
    end

    # Constraints to speed up the solving process
    # Last adder is equal to an output
    @constraint(model, ca[end] <= maximum(C))
    # if known_min_NA == NA
    #     @constraint(model, ca[end] == sum(C[j]*oaj[end,j] for j in 1:NO))
    # end
    # # At least x adders should be equal to an output at adder n
    # @constraint(model, [n in 1:NA], sum(oaj[a,j] for a in 1:n, j in 1:NO) >= NO - (NA-n))
    # # Adders are outputs or used for following ones or not used
    # @constraint(model, [a in 1:(known_min_NA-1)], sum(oaj[a,j] for j in 1:NO) +
    #     sum(caik[var_adder,i,a] for var_adder in (a+1):NA, i in 1:2) >= 1)
    # if known_min_NA < NA
    #     @constraint(model, sum(oaj[known_min_NA,j] for j in 1:NO) +
    #         sum(caik[var_adder,i,known_min_NA] for var_adder in (known_min_NA+1):NA, i in 1:2) >= 1)
    #     @constraint(model, [a in (known_min_NA+1):(NA-1)], sum(oaj[a,j] for j in 1:NO) +
    #         sum(caik[var_adder,i,a] for var_adder in (a+1):NA, i in 1:2) >= used_adder[a])
    # end

    if use_symmetry_breaking_cst || minimize_adderdepth || adder_depth_max != 0
        @constraint(model, [a in 2:NA, j in 1:NO], ada[a] >= oaj[a,j]*get_min_number_of_adders(C[j]))
    end

    # Symmetry breaking
    if use_symmetry_breaking_cst
        if known_min_NA < NA
            if known_min_NA >= start_sym_break+1
                @constraint(model, [a in start_sym_break:(known_min_NA-1)], ada[a] <= ada[a+1])
                @constraint(model, [a in start_sym_break:(known_min_NA-1)], ca[a] <= ca[a+1] + maximum_value*(ada[a+1]-ada[a]))
            end
            if NA >= start_sym_break+1
                @constraint(model, [a in max(known_min_NA,start_sym_break):(NA-1)], ada[a] <= ada[a+1] + NA*(1-used_adder[a+1]))
                @constraint(model, [a in max(known_min_NA,start_sym_break):(NA-1)], ca[a] <= ca[a+1] + maximum_value*(ada[a+1]-ada[a]) + NA*maximum_value*(1-used_adder[a+1])) # ada[a+1]-ada[a] <= 0 if adder a+1 not used
            end
        else
            if NA >= start_sym_break+1
                @constraint(model, [a in start_sym_break:(NA-1)], ada[a] <= ada[a+1])
                @constraint(model, [a in start_sym_break:(NA-1)], ca[a] <= ca[a+1] + maximum_value*(ada[a+1]-ada[a]))
            end
        end
    end


    if minimize_adderdepth
        if known_min_NA < NA
            @objective(model, Min, NA*(known_min_NA+sum(used_adder)) + max_ad)
        else
            @objective(model, Min, max_ad)
        end
    else
        if !avoid_internal_shifts
            if known_min_NA < NA
                @objective(model, Min, known_min_NA+sum(used_adder))
            else
                # Mock objective
                @objective(model, Min, NA)
            end
        else
            # Objective that permit to minimize the number of inside shifts
            if known_min_NA < NA
                @objective(model, Min, NA*sum(used_adder) +
                    sum(Psias[a,s] for a in 1:NA, s in Smin:-1))
            else
                @objective(model, Min, sum(Psias[a,s] for a in 1:NA, s in Smin:-1))
            end
        end
    end

    return model
end




"""
    optimize_increment!(model::Model, model_mcm_forumlation!::Function,
                        C::Vector{Int}, wordlength::Int, S::Tuple{Int, Int},
                        verbose::Bool)::Model

Increment NA until a solution is found for the coefficients in `C`.
"""
function optimize_increment!(model::Model, model_mcm_forumlation!::Function,
                             C::Vector{Int}, wordlength::Int, S::Tuple{Int, Int},
                             ;verbose::Bool = false,
                             nb_adders_lb::Int = 0, kwargs...)::Model
    NA = max(get_min_number_of_adders(C), nb_adders_lb)
    model_mcm_forumlation!(model, C, wordlength, S, NA; verbose=verbose, known_min_NA=get_min_number_of_adders(C), kwargs...)
    timelimit = time_limit_sec(model)
    total_solve_time = 0.0
    optimize!(model)
    current_solve_time = solve_time(model)
    total_solve_time += current_solve_time
    while termination_status(model) in [MOI.INFEASIBLE, MOI.INFEASIBLE_OR_UNBOUNDED]
        timelimit -= current_solve_time
        verbose && println("$(termination_status(model)) for NA = $NA in $current_solve_time seconds")
        if timelimit <= 0.0
            break
        end
        NA += 1
        empty!(model)
        model_mcm_forumlation!(model, C, wordlength, S, NA; verbose=verbose, known_min_NA=NA, kwargs...)
        set_time_limit_sec(model, timelimit)
        optimize!(model)
        current_solve_time = solve_time(model)
        total_solve_time += current_solve_time
    end
    model[:NA] = Vector{Int}()
    count_solution = 0
    while has_values(model; result=count_solution+1)
        count_solution += 1
        push!(model[:NA], sum(round(value(model[:ca][a])) != 1 ? 1 : 0 for a in 1:NA))
    end
    if !isempty(model[:NA])
        NA = model[:NA][1]
    end
    verbose && println("\n$(termination_status(model)) for NA = $NA in $current_solve_time seconds\nTotal time: $total_solve_time seconds\n\n\n")
    # println(value.(model[:ca]))
    # println(value.(model[:ada]))

    return model
end
